#!/usr/bin/env python3
# This script calculates the filesize and checksum of the compressed & uncompressed image files.
#
# It requires the following files to be present:
# - the compressed image at
#   `deploy/image_xxx.zip`
# - the checksum file for the compressed image at
#   `deploy/sha256sums`
#
# Inside the image zip, there must be the uncompressed image: `xyz.img`
#
# It writes the output to `deploy/image_metadata.json`

import os
import json
import zipfile
import subprocess

from pathlib import Path
from tempfile import TemporaryDirectory

SIZES_AND_CHECKSUMS_FILENAME = 'deploy/image_metadata.json'


def calculate_sizes_and_checksums():
    """
    """
    # example: "./deploy/image_2020-12-27-Raspbian-SatNOGS-lite.zip"
    image_download_path = next(Path("./deploy/").glob("image_*.zip"))

    # Read checksum from file
    checksums = Path("./deploy/sha256sums").read_text()
    image_download_sha256 = checksums.split('\n')[1].split()[0]
    # Get filesize from filesystem
    image_download_size = os.path.getsize(image_download_path)

    # Extract image
    with TemporaryDirectory() as temp_dir:
        with zipfile.ZipFile(image_download_path, 'r') as ff:
            ff.extractall(temp_dir)

        # Example: './tmp/2020-12-27-Raspbian-SatNOGS-lite.img'
        image_extract_path = next(Path(temp_dir).glob("*.img"))

        # Get filesize from filesystem
        extract_size = os.path.getsize(image_extract_path)

        # Calculate checksum
        checksum_process = subprocess.run(["sha256sum", image_extract_path], capture_output=True)
        extract_sha256 = checksum_process.stdout.decode('ascii').split()[0]

    return {
        'extract_size': extract_size,
        'extract_sha256': extract_sha256,
        'image_download_size': image_download_size,
        'image_download_sha256': image_download_sha256
    }

if __name__ == '__main__':
    print("Calculating compressed & uncompressed image file sizes and checksums...")
    entry0_metadata = calculate_sizes_and_checksums()
    with open(SIZES_AND_CHECKSUMS_FILENAME, 'w') as entry0_metadata_f:
        json.dump(entry0_metadata, entry0_metadata_f, indent=2)
    print("Done")
