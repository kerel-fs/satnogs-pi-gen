#!/usr/bin/env python3

# This script generates the file `rpi-imager.json` needed by rpi-imager (the Raspberry Pi Imaging Utility,
# https://github.com/raspberrypi/rpi-imager) to know about the SatNOGS OS Images.
#
# Input:
# - env variable: CI_COMMIT_TAG
# - file: ./rpi-imager/additional_images.json
# - file: ./deploy/image_metadata.json

# Output:
# - file: ./public/rpi-imager.json

import os
import json

from pathlib import Path

CI_COMMIT_TAG = os.environ['CI_COMMIT_TAG']
IMG_DATE = f"{CI_COMMIT_TAG[:4]}-{CI_COMMIT_TAG[4:6]}-{CI_COMMIT_TAG[6:8]}"

SATNOGS_IMAGE_NAME = "SatNOGS (stable)"
SATNOGS_IMAGE_DESCRIPTION = f"Raspbian-based SatNOGS Image {CI_COMMIT_TAG}"
SATNOGS_IMAGE_URL = "https://gitlab.com/librespacefoundation/satnogs/satnogs-pi-gen/" \
                    f"-/jobs/artifacts/{CI_COMMIT_TAG}/raw/deploy/" \
                    f"image_{IMG_DATE}-Raspbian-SatNOGS-lite.zip" \
                    "?job=release&workaround=.zip"
SATNOGS_IMAGE_RELEASE_DATE = IMG_DATE
SATNOGS_ICON_URL = "https://librespacefoundation.gitlab.io/satnogs/satnogs-pi-gen/satnogs-logo-40px.png"

SIZES_AND_CHECKSUMS_FILENAME = 'deploy/image_metadata.json'
ADDITIONAL_IMAGES_JSON_FILENAME = './rpi-imager/additional_images.json'

# Output:
RPI_IMAGER_JSON_PATH = "./public/rpi-imager.json"


if __name__ == "__main__":
    if not Path(SIZES_AND_CHECKSUMS_FILENAME).exists():
        print("Missing image metadata file: {SIZES_AND_CHECKSUMS_FILENAME}")
        sys.exit(-1)

    with open(SIZES_AND_CHECKSUMS_FILENAME, 'r') as entry0_metadata_f:
        entry0_metadata = json.load(entry0_metadata_f)

    entry0 = {
        "name": SATNOGS_IMAGE_NAME,
        "description": SATNOGS_IMAGE_DESCRIPTION,
        "url": SATNOGS_IMAGE_URL,
        "icon": SATNOGS_ICON_URL,
        "release_date": SATNOGS_IMAGE_RELEASE_DATE,
    }
    entry0.update(entry0_metadata)

    data = {
      "os_list": [
          entry0
      ]
    }

    with open(ADDITIONAL_IMAGES_JSON_FILENAME, 'r') as additional_images_f:
        additional_images = json.load(additional_images_f)
    data['os_list'].extend(additional_images)

    with open(RPI_IMAGER_JSON_PATH, 'w') as output_file:
        json.dump(data, output_file, indent=2)
