#!/bin/bash -e

install -m 644 files/satnogs-setup.conf ${ROOTFS_DIR}/etc/default/satnogs-setup

install -m 755 files/satnogs-setup.sh ${ROOTFS_DIR}/usr/local/bin/satnogs-setup
install -m 755 files/satnogs-upgrade.sh ${ROOTFS_DIR}/usr/local/bin/satnogs-upgrade

install -d ${ROOTFS_DIR}/usr/local/share/satnogs-setup
install -m 755 files/bootstrap.sh ${ROOTFS_DIR}/usr/local/share/satnogs-setup/bootstrap.sh
install -m 755 files/config.sh ${ROOTFS_DIR}/usr/local/share/satnogs-setup/config.sh

install -d ${ROOTFS_DIR}/etc/ansible
install -m 644 files/hosts ${ROOTFS_DIR}/etc/ansible/hosts
